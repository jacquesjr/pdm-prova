package br.edu.iftm.pdm.prova.ui.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import br.edu.iftm.pdm.prova.R;
import br.edu.iftm.pdm.prova.data.DAORegistersSingleton;
import br.edu.iftm.pdm.prova.model.*;
import br.edu.iftm.pdm.prova.ui.activities.MainActivity;
import br.edu.iftm.pdm.prova.ui.list_builders.RegistersListBuilder;

public class RegistersListerFragment extends Fragment
        implements RegistersListerFragment.RegistersListBuilder {

    public static final String TAG = "StoreListFragment";
    public static final String COSTUMER_USER_KEY = "StoreListFragment.COSTUMER_USER";

    public RegistersListerFragment() {
        super(R.layout.fragment_list_register);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        (new RegistersListBuilder(this.getActivity(), R.id.rvRegisters))
                .RegistersListBuilderListener(this)
                .load(DAORegistersSingleton.getINSTANCE().getRegisters());
    }

    @Override
    public void onRequestRegisters(Registers store) {
        Bundle storeBundle = new Bundle();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fcvMain, MenuBookFragment.class, storeBundle, MenuBookFragment.TAG)
                .addToBackStack(MainActivity.MAIN_BACK_STACK_KEY)
                .commit();
    }
}

