package br.edu.iftm.pdm.prova.ui.list_builders;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.IdRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.edu.iftm.pdm.prova.model.*;
import br.edu.iftm.pdm.prova.model.Registers;
import br.edu.iftm.pdm.prova.ui.lists.RegistersDataAdapter;


public class RegistersListBuilder {

    private RecyclerView rvRegisters;
    private LinearLayoutManager layoutManager;
    private RegistersDataAdapter adapter;

    public interface RegistersListBuilderListener {
        void onRequestRegisters(Registers register);
    }

    public RegistersListBuilder(Activity activity, @IdRes int rvRegisters) {
        this.rvRegisters = activity.findViewById(rvRegisters);
        this.layoutManager = new LinearLayoutManager(activity);
        this.rvRegisters.setLayoutManager(this.layoutManager);
        this.adapter = null;
    }

    public RegistersListBuilder load(ArrayList<Registers> orders) {
        this.adapter = new RegistersDataAdapter(orders);
        this.rvRegisters.setAdapter(this.adapter);
        return this;
    }

}