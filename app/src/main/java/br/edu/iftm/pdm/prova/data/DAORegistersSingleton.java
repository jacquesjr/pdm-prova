package br.edu.iftm.pdm.prova.data;

import java.util.ArrayList;

import br.edu.iftm.pdm.prova.model.Registers;

public class DAORegistersSingleton {
    private static DAORegistersSingleton INSTANCE;
    private ArrayList<Registers> registers;

    private DAORegistersSingleton() {
        this.registers = new ArrayList<>();
    }

    public static DAORegistersSingleton getINSTANCE() {
        if(INSTANCE == null)
            INSTANCE = new DAORegistersSingleton();
        return INSTANCE;
    }

    public ArrayList<Registers> getRegisters() {
        return registers;
    }

    public void addOrder(Registers register) {
        this.registers.add(register);
    }
}
