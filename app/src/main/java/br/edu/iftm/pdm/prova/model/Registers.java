package br.edu.iftm.pdm.prova.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Registers {

    public static final int CREDIT = 0;
    public static final int DEBIT = 1;
    private static int SERIAL = 0;

    private int id;
    private int value;
    private String description;
    private int registerType;

    public Registers(int value, String description, int registerType) {
        this.id = SERIAL;
        this.value = value;
        this.description = description;
        this.registerType = registerType;
    }

    public int getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public int getRegisterType() {
        return registerType;
    }


    protected Registers(Parcel in) {
        value = in.readInt();
        description = in.readString();
        registerType = in.readInt();
    }
}
