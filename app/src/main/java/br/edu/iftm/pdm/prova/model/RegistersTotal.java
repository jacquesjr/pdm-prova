package br.edu.iftm.pdm.prova.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class RegistersTotal implements Parcelable {

    private static int SERIAL = 0;

    private int id;
    private ArrayList<Registers> registersList;
    private int total;

    public RegistersTotal() {
        this.registersList = new ArrayList<>();
        this.total = 0;
        SERIAL++;
    }

    public int getId() {
        return id;
    }

    public ArrayList<Registers> getOrderItems() {
        return registersList;
    }

    public void addOrderItem(Registers register) {
        this.registersList.add(register);
    }

    public int getRegisterType() {
        return total;
    }

    public void setRegisterType(int registerType) {
        this.total = registerType;
    }

    public void getValue() {
        for (Registers item : this.registersList) {
            if(item.getRegisterType() == 0){
                this.total += item.getValue();
            } else {
                this.total -= item.getValue();
            }
        }
    }

    public boolean isValid() {
        return !this.registersList.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(Registers item : this.registersList) {
            builder.append(item.toString());
        }
        builder.append("Total:\n");
        this.getValue();
        builder.append(this.total);
        return builder.toString();
    }

    protected RegistersTotal(Parcel in) {
        id = in.readInt();
        total = in.readInt();
    }

    public static final Creator<RegistersTotal> CREATOR = new Creator<RegistersTotal>() {
        @Override
        public RegistersTotal createFromParcel(Parcel in) {
            return new RegistersTotal(in);
        }

        @Override
        public RegistersTotal[] newArray(int size) {
            return new RegistersTotal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(total);
    }
}
