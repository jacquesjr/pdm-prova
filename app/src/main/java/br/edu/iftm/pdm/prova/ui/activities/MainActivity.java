package br.edu.iftm.pdm.prova.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import br.edu.iftm.pdm.prova.R;
import br.edu.iftm.pdm.prova.ui.fragments.RegistersListerFragment;

public class MainActivity extends AppCompatActivity {

    public static final String MAIN_BACK_STACK = "MainActivity.MAIN_BACK_STACK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bnv = findViewById(R.id.bnavManager);
        NavController navController = Navigation.findNavController(this, R.id.fragment);

        NavigationUI.setupWithNavController(bnv, navController);
    }
}