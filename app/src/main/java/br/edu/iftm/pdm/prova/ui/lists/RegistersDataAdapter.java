package br.edu.iftm.pdm.prova.ui.lists;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.edu.iftm.pdm.prova.R;
import br.edu.iftm.pdm.prova.model.Registers;

public class RegistersDataAdapter extends RecyclerView.Adapter<RegistersDataViewHolder> {

    private ArrayList<Registers> registers;
    private OnClickEditStatusListener listener;
    private SparseBooleanArray toggleInfo;

    public interface OnClickEditStatusListener {
        void onClickEditOrder(Registers register, int position);
    }

    public RegistersDataAdapter(ArrayList<Registers> registers) {
        this.registers = registers;
        this.toggleInfo = new SparseBooleanArray();
    }

    @NonNull
    @Override
    public RegistersDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_view_register, parent, false);
        return new RegistersDataViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(@NonNull RegistersDataViewHolder holder, int position) {
        holder.bind(this.registers.get(position), position);
    }

    @Override
    public int getItemCount() {
        return this.registers.size();
    }
}
