package br.edu.iftm.pdm.prova.ui.lists;

import android.content.res.Resources;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.edu.iftm.pdm.prova.R;
import br.edu.iftm.pdm.prova.model.Registers;

public class RegistersDataViewHolder extends RecyclerView.ViewHolder {

    private Registers currentRegister;
    private final TextView txtRegistersLabelPreview;
    private final FrameLayout flRegistersPreview;
    private final RegistersDataAdapter adapter;
    private int position;

    public RegistersDataViewHolder(@NonNull View itemView, RegistersDataAdapter adapter) {
        super(itemView);
        this.txtRegistersLabelPreview = itemView.findViewById(R.id.txtRegisterLabelPreview);
        this.flRegistersPreview = itemView.findViewById(R.id.flRegistersPreview);
        this.adapter = adapter;
    }

    public void bind(Registers register, int position) {
        this.currentRegister = register;
        this.position = position;
        String registerLabel = itemView.getContext().getString(R.string.registers);
        registerLabel += " #" + register.getDescription();
        this.txtRegistersLabelPreview.setText(registerLabel);
        this.updateTypeRegister();
    }

    private void updateTypeRegister() {
        int color;
        Resources res = itemView.getContext().getResources();
        switch (this.currentRegister.getRegisterType()) {
            case Registers.CREDIT:
                color = res.getColor(R.color.green, null);
                break;
            case Registers.DEBIT:
                color = res.getColor(R.color.red, null);
                break;
            default:
                color = res.getColor(R.color.gold, null);
                break;
        }
        this.flRegistersPreview.setBackgroundColor(color);
    }
}
